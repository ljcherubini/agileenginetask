package com.ljc.agileenginetask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            Document document = openDocument(args, 0);

            Elements makeEverythingOkButtons = document.select("div[class^=\"panel panel-default\"]:contains(Make Everything OK Area) > div > a");

            System.out.println("Occurrences:");
            makeEverythingOkButtons.forEach(e -> {
                System.out.println("---------------");
                System.out.println("Tag: " + e.tag());
                System.out.println("Id: " + e.id());
                System.out.println("Text: " + e.text());
                System.out.println("Classes: " + e.classNames());
                System.out.println("HTML: " + e.toString());
                System.out.println("Selector: " + e.cssSelector());

            });
        } catch (IOException e) {
            System.out.println("Error while reading document");
        }

    }

    private static Document openDocument(String[] args, int index) throws IOException {
        Scanner scanner = new Scanner(System.in);
        File htmlFile;
        if(args.length > index) {
            htmlFile = new File(args[index]);
        } else {
            System.out.println("Please, enter the file location:");
            htmlFile = new File(scanner.nextLine());
        }

        return Jsoup.parse(htmlFile, "UTF-8");
    }
}
